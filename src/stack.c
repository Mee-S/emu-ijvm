#include <stack.h>
#include <frame.h>


void init_stack() 
{
	stack.pointer = 0;
	stack.size = INIT_STACK_SIZE;
	stack.data = malloc(INIT_STACK_SIZE * sizeof(word_t));
	// check if there's enough space to do malloc
	assert(stack.data != NULL);
}

word_t pop() 
{
	assert( stack.pointer > last_frame()->stack_pointer );
	
	if (stack.pointer < (stack.size / 2))
	{
		stack.size /= 2;
		stack.data = (word_t*)realloc(stack.data, (size_t)stack.size * sizeof(word_t));
		assert(stack.data != NULL);
	}
	
	return stack.data[--stack.pointer];
}

void push(word_t num) 
{
	if(stack.pointer >= stack.size)
	{
		stack.size *= 2;
		stack.data = realloc(stack.data, (size_t)stack.size * sizeof(word_t));
		assert(stack.data);
	}
	stack.data[stack.pointer++] = num;
}

word_t tos()
{
	assert(stack.pointer > last_frame()->stack_pointer);
	
	return stack.data[stack.pointer - 1];
}

void free_stack()
{
	// only stack.data was dynamically allocated
	if(stack.data)
		free(stack.data);
}
