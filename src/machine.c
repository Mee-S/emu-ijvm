#include <general.h>

int program_counter;				
FILE *output;			
FILE *input;			


word_t bytes_to_word(byte_t *bytes)
{
	word_t result = (word_t)((bytes[0] << 24) | (bytes[1] << 16 ) | (bytes[2] << 8) | bytes[3]);
	return result;
}

short bytes_to_short(byte_t *bytes)
{
	short result = (short)((bytes[0] << 8) | bytes[1]);
	return result;
}

unsigned short bytes_to_u_short(byte_t *bytes)
{
	unsigned short result = (unsigned short)((bytes[0] << 8) | bytes[1]);
	return result;
}

int text_size()
{
	return text->size;
}

word_t get_constant(int i)
{
	long offset;
	// checks if parameter is in constants scope
	assert(i < const_block->size);

	offset = i * (int)sizeof(word_t);
	return bytes_to_word(&const_block->data[offset]);
}

word_t get_local_variable(int i)
{
	return last_frame()->vars[i];
}

void iinvoke()
{
	Frame *f;									
	int current_program_counter;									
	int args_count; 		// number of arguments method expects								
	int local_variables;   // local variable area size								
	int constant_location;								
	int i;

	program_counter += BYTE_LENGHT;
	current_program_counter = program_counter + BYTE_LENGHT;
	constant_location = bytes_to_u_short(&text->data[program_counter]); 
	program_counter = get_constant(constant_location);						
	args_count = bytes_to_u_short(&text->data[program_counter]);
	program_counter += SHORT_LENGHT;
	
	local_variables = bytes_to_u_short(&text->data[program_counter]);
	f = add_frame(local_variables+args_count, current_program_counter, stack.pointer - args_count);
	for(i = args_count-1; i > 0; i--)				
		f->vars[i] = pop();

	last_frame()->next = f;						
	program_counter++;
}

void ireturn()
{
	Frame *f;									
	word_t return_store;							

	f = last_frame();
	return_store = tos();

	// set program_counter to frame's
	program_counter = f->pc;									

	// pop everything until the previous value is reached
	while(stack.pointer > f->stack_pointer)	
		pop();

	remove_last_frame();		
	// push return value at the top of the stack				
	push(return_store);							
}

bool execute_wide()
{
	program_counter += BYTE_LENGHT;
	switch(text->data[program_counter])
	{
		case OP_BIPUSH:
			push(bytes_to_short(&text->data[program_counter + 1]));
			program_counter += SHORT_LENGHT;
		break;
		case OP_ILOAD:
			push(get_local_variable(bytes_to_u_short(&text->data[program_counter + 1])));
			program_counter += SHORT_LENGHT;
		break;
		case OP_ISTORE:
			store(bytes_to_u_short(&text->data[program_counter + 1]));
			program_counter += SHORT_LENGHT;
		break;
		case OP_IINC:
			iinc(bytes_to_short(&text->data[program_counter + 3]), bytes_to_short(&text->data[program_counter + 1]));
			program_counter += WORD_LENGHT;
		break;
		default:
			return false;
		break;
	}
	return true;
}

bool execute_instruction()
{
	switch(text->data[program_counter])
	{
		case OP_BIPUSH:
			push((int8_t)text->data[program_counter + 1]);
			program_counter += BYTE_LENGHT;
			break;
		case OP_DUP: 
			idup();
			break;
		case OP_ERR:
			return false;
			break;
		case OP_GOTO: {
			program_counter += bytes_to_short(&text->data[program_counter + 1]) - BYTE_LENGHT;
			break;
		}
		case OP_HALT:
			return false;
			break;
		case OP_IADD: {
			iadd();
			break;
		}
		case OP_IAND: {
			iand();
			break;
		}
		case OP_IFEQ: {
			// pop one word and check if it's zero
			if((pop() == 0)) {
				program_counter += bytes_to_short(&text->data[program_counter + 1]) - BYTE_LENGHT;
			}
			else
			{
				program_counter += SHORT_LENGHT;
			}
			break;
		}
		case OP_IFLT: {
			// pop one word and check if it's less than zero
			if((pop() < 0)) {
				program_counter += bytes_to_short(&text->data[program_counter + 1]) - BYTE_LENGHT;
			}
			else
			{
				program_counter += SHORT_LENGHT;
			}
			break;
		}
		case OP_ICMPEQ: {
			// pops two words and compares them if they are equal
			word_t a, b;
			a = pop();
			b = pop();
			if(a == b) {
				program_counter += bytes_to_short(&text->data[program_counter + 1]) - BYTE_LENGHT;
			}
			else {
				program_counter += SHORT_LENGHT;
			}
			break;
		}
		case OP_IINC:
			// adds constant value to local variable
			iinc((int8_t)text->data[program_counter+2], (int8_t)text->data[program_counter + 1]);
			program_counter += SHORT_LENGHT;
			break;
		case OP_ILOAD:
			// adds local variable to stack
			push(get_local_variable(text->data[program_counter + 1]));
			program_counter += BYTE_LENGHT;
			break;
		case OP_IN: {
			iinput();
			break;
		}
		case OP_INVOKEVIRTUAL:
			iinvoke();
			break;
		case OP_IOR: {
			word_t a, b;
			a = pop();
			b = pop();
			push(b | a);
			break;
		}
		case OP_IRETURN:
			ireturn();
			break;
		case OP_ISTORE:
			store(text->data[program_counter + 1]);
			program_counter += BYTE_LENGHT;
			break;
		case OP_ISUB: {
			isub();
			break;
		}
		case OP_LDC_W:
			push(get_constant(bytes_to_u_short(&text->data[program_counter + 1])));
			program_counter += SHORT_LENGHT;
			break;
		case OP_NOP:
			break;
		case OP_OUT: {
			ioutput();
			break;
		}
		case OP_POP:
			pop();
			break;
		case OP_SWAP: {
			iswap();
			break;
		}
		case OP_WIDE:
			execute_wide();
			break;
		default:
			return false;
	}
	return true;
}

bool step()
{
	bool halt = execute_instruction();
	program_counter++;
	return halt;
}

byte_t *get_text()
{
	return text->data;
}

int get_program_counter()
{
	return program_counter;
}

byte_t get_instruction(void)
{
	return text->data[program_counter];
}

int init_ijvm(char *binary_file)
{
	FILE *binary;

	if((binary = fopen(binary_file, "rb")) == NULL)
		return -1;

	word_t magic;

	// read and check if it contains magic number
	fread(&magic, sizeof(word_t), 1, binary);
	magic = (word_t)bswap_32((uint32_t)magic);

	if(magic != MAGIC_NUMBER)
		return -1;

	program_counter = 0;

	// default I/O is terminal
	set_input(stdin);						
	set_output(stdout);						

	init_block(binary);
	root = init_frame();				
	init_stack();							

	fclose(binary);			

	return 0;
}

void destroy_ijvm() {
	free_stack();
	free_blocks();
	free_frames();
}

void run() {
	// steps until end of file 
	do {
	}
	while(step() && program_counter <= text->size);
}

void set_input(FILE *fp)
{
	input = fp;
}

void set_output(FILE *fp)
{
	output = fp;
}

FILE * get_output()
{
	 return output;
}

FILE * get_input()
 {
	 return input;
}
