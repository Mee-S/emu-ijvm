#include <instruction.h>

void iadd()
{
	word_t a, b;
	a = pop();
	b = pop();
	push(a + b);
}

void isub() 
{
	word_t a, b;
	a = pop();
	b = pop();
	push(b - a);
}

void iand() 
{
	word_t a, b;
	a = pop();
	b = pop();
	push(b & a);
}

void ior() 
{
	word_t a, b;
	a = pop();
	b = pop();
	push(b | a);
}

void iswap() 
{
	word_t a, b;
	a = pop();
	b = pop();
	push(a);
	push(b);
	
}

void idup()
{
	push(tos());
}

void iinput() 
{
	int input;
	
	input = fgetc(get_input());
	if(input == -1)
		push(0);
	else
		push(input);
}

void ioutput() 
{
	char a = (char)pop();
	fputc(a, get_output());	
}
