#include <block.h>

Block * load_block(FILE *f)
{
	Block * b;
	size_t size;

	// dynamically allocate memory for the block
	b = (Block*)malloc(sizeof(Block));

	// reads size from file
	fread(&size, sizeof(word_t), 1, f);		
	fread(&size, sizeof(word_t), 1, f);		
	// byte swap size
	size = (size_t)bswap_32((uint32_t)size);
	// converts size to its type - word_t
	b->size = (word_t)size;
	// dynamically allocate memory for data in block
	b->data = (byte_t*)malloc(size * sizeof(byte_t));

	// read data and add them to previously taken memory
	fread(b->data,sizeof(byte_t), size, f);	
	return b;
}

void init_block(FILE *binary) {
	// loads constants and text from file
  const_block = load_block(binary);	
	text = load_block(binary);	
}

void free_blocks()
{
	// free dynamically allocated memory
  if(text->data)
    free(text->data);
	if(const_block->data)
		free(const_block->data);
	if(text) {
		free(text);
    text = NULL; 
  }
	if(const_block) {
		free(const_block);
    const_block = NULL; 
  }
}
