#include <frame.h>

Frame * last_frame() 
{
	Frame *f;
	
	f = root;
	// loops until it finds last frame
	while(f->next != NULL)
		f = f->next;
	
	return f;
}

Frame * frame_before_last() 
{
	Frame *f;
	
	f = root;
	// loop until next frame doesn't have the next
	while(f->next->next != NULL)
		f = f->next;
	
	return f;
}

Frame * init_frame() {
	Frame *f;
	
	// init frame and allocate memory - set everything to 0
	f = (Frame*)malloc(sizeof(Frame));
	
	f->vars = malloc((size_t)(0) * sizeof(word_t));
	f->pc = 0;
	f->var_size = 0;
	f->stack_pointer = 0;
	f->next = NULL;
	
	return f;
}

Frame * add_frame(int local_vars, int currentpc, int stackptr)
{
	Frame *f;
	
	f = (Frame*)malloc(sizeof(Frame));
	
	// dynamically allocate memory for vars
	f->vars = malloc((size_t)(local_vars) * sizeof(word_t));
	
	f->pc = currentpc;
	f->var_size = local_vars;
	f->stack_pointer = stackptr;
	f->next = NULL;
	
	return f;
}

void store(int i)
{
	Frame *f;
	
	f = last_frame();
	
	if((i + 1) > f->var_size)				
	{
		f->var_size = (i + 1);
		// we need more space for new var so we use realloc
		f->vars = realloc(f->vars, (size_t)(f->var_size) * sizeof(word_t));
	}
	f->vars[i] = pop();
}

void iinc(int num, int var)
{
	Frame *f = last_frame();
	
	if((var + 1) > f->var_size)					
	{
		f->var_size = (var + 1);
		f->vars = realloc(f->vars, (size_t)(f->var_size) * sizeof(word_t));
	}
	f->vars[var] += num;
}

void remove_last_frame()
{
	Frame* f;
	
	f = last_frame();
	frame_before_last()->next = NULL;
	
	// free memory only for this frame
	if(f->vars)
		free(f->vars);
	if(f)
		free(f);
}

void free_frames()
{
	// only free memory that was dynamically allocated
	Frame* tmp;
	Frame* head;
	
	head = root;
	while(head)
	{
		tmp = head;
		head = head->next;
		if(tmp->vars)
			free(tmp->vars);
		if(tmp)
			free(tmp);
	}
}
