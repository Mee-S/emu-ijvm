
#ifndef FRAME_H
#define FRAME_H
#include <ijvm.h>
#include <stdlib.h>
#include <assert.h>
#include <stack.h>

typedef struct Frame 
{
	word_t *vars;  // variables in frame
	int pc;	// frame program counter
	int stack_pointer;
	int	var_size;
	struct Frame *next; // holds next frame in list
}Frame;



Frame* root;													


/**
 * Returns the current frame of the program
 **/
Frame * last_frame(void);

/**
 * Return the frame before the last
 **/
Frame * frame_before_last(void);

Frame *add_frame(int, int, int);

// Creates initial frame - root frame
Frame *init_frame();

/**
 * Store a local variable at index i
 **/
void store(int i);

/**
 * Increase the value of a local variable by a constant value
 **/
void iinc(int num, int var);

/**
 * destory all the frames
 **/
void free_frames(void);

/**
 * Removes the last frame in the linked list
 **/
void remove_last_frame(void);

#endif
