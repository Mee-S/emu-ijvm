#ifndef INSTRUCTIONS_H
#define INSTRUCTIONS_H

#include <ijvm.h>
#include <stack.h>
#include <general.h>


/**
 * Pops two words from stack and pushes their sum 
 **/
 void iadd(void);		
 
/**
 * Pops two words from stack and pushes their diff
 **/
void isub(void);		

/**
 * Copy first word on stack and pushes it
 **/
void idup(void);		

/**
 * Pops two words from stack and pushes AND 
 **/
void iand(void);		

/**
 * Pops two words from stack and pushes OR 
 **/
void ior(void);

/**
 * Swaps two first words on stack
 **/
void iswap(void);

/**
 * Reads a character from the input and pushes it onto the stack. If no character is available, 0 is pushed 
 **/
void iinput(void);

/**
 * Pops first word from stack and prints it
 **/
void ioutput(void);

#endif
