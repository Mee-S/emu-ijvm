#ifndef MACHINE_H
#define MACHINE_H
#include <ijvm.h>
#include <stdlib.h>
#include <frame.h>
#include <byteswap.h>
#include <block.h>
#include <instruction.h>

#define WORD_LENGHT  4
#define SHORT_LENGHT 2
#define BYTE_LENGHT  1



/**
 * Converts bytes to a word
 **/
word_t bytes_to_word(byte_t *bytes);

/**
 * converts bytes to a short
 **/
short bytes_to_short(byte_t *bytes);

/**
 * converts bytes to an unsigned short
 **/
unsigned short bytes_to_u_short(byte_t *bytes);

/**
 * Invokes new method, creates new frame
 **/
void iinvoke(void);

/**
 * Returns from method, removes the last frame
 **/
void ireturn(void);

/**
 * Executes a wide instruction, return false if failed
 **/
bool execute_wide(void);

/**
 * Executes and instruction, return false if failed, or HALT
 **/
bool execute_instruction(void);

/**
 * Returns the output device
 **/
FILE * get_output(void);

/**
 * Returns the input device
 **/
FILE * get_input(void);

#endif
