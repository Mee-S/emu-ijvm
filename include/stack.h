#ifndef STACK_H
#define STACK_H

#include <assert.h>
#include <ijvm.h>
#include <stdlib.h>


#define INIT_STACK_SIZE 16

typedef struct Stack {
	word_t *data;
	int pointer;
	int size;
}Stack;

Stack stack;



void init_stack(void);

word_t pop(void);

void push(word_t num);

void free_stack(void);

#endif
