#ifndef BLOCK_H
#define BLOCK_H
#include <ijvm.h>
#include <stdlib.h>
#include <byteswap.h>

typedef struct Block {
	byte_t *data;
	word_t size;
}Block;

Block *text;		//Block containting the text of the program
Block *const_block;		//Block containing the constants of the program

/**
 * Loads a block from the binary, returns the block
 **/
Block * load_block(FILE *f);

/**
 * Frees the alloced arrays of the blocks
 **/
void free_blocks(void);

// Gets blocks from the file input
void init_block(FILE *f);
#endif
